# TIPS AND TRICKS

A collection of useful commands I discovered/developed over time.

Contents:

[TOC]

Other:

- [Apps.md](Apps.md)

# ASCII/Unicode

## Convert end-of-line characters

To convert CRLF to LF, and vice versa:

```bash
dos2unix some_file.txt
unix2dos some_file.txt
```

## Find all non-ASCII characters in a file

```bash
grep --color='auto' -P -n '[^\x00-\x7F]' SRCFILE
```

## Convert all non-ASCII characters to ASCII equivalents

```bash
iconv --from-code UTF-8 --to-code US-ASCII//translit -c SRCFILE > DSTFILE
```

# Programming

## JavaScript

### Trap `alert()` calls before they happen

```JavaScript
window.alert = function() { debugger; }; // Trick to trap alert() calls.
```

# Revision Control

## Git

### Fix wrong username/email in submitted changelists

From [GitHub](https://help.github.com/articles/changing-author-info/):

```bash
#!/bin/sh

git filter-branch -f --env-filter '

OLD_EMAIL="furst.last@dumbain.dom"
CORRECT_NAME="FirstName LastName"
CORRECT_EMAIL="first.last@domain.dom"

if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi

' --tag-name-filter cat -- --branches --tags
```

Note that I've added a `-f` flag to allow fixing multiple cases before submitting.

Once it is fixed (use `git log` to confirm), they recommend pushing using:

```bash
git push --force --tags origin 'refs/heads/*'
```

### Skip fast-forward when merging a branch

By default, Git does a fast-forward on a merge, meaning that the following history:

```text
0 ------------> 4  (master)
 \             /
  1 --> 2 --> 3    (someBranch)
```

would get fast-forwarded to:

```text
0 --> 1 --> 2 --> 3 (master)
```

If you want to keep the commit log for the change `4` above, call this:

```bash
git merge someBranch --no-ff
```

This is the preferred approach when associating a branch to issues (or merge requests).

### Clean up old branches

Sync with the remote server while removing remote tracking branches that are no longer present on the server (e.g. `origin/some-old-branch`):

```bash
git fetch -p
```

List all of the local branches that have been merged:

```bash
git checkout master
git branch --merged
```

Remove those branches locally:

```bash
git branch -d some-old-branch
```

The only downside is that while you git history will still see the branching, the branch names will now be missing (mostly aesthetic loss).

## Mercurial

### Find what a specific person committed in the last week

```bash
hg log --user "Someone" -d "-6"
```

# Scripting

## Bash

### Call a command the proper way

```bash
PLAT=$(uname -o)
```

No more backticks such as:

```bash
PLAT=`uname -o`
```

as this old approach doesn't allow to nest commands in commands.

### Determining a script's directory

First of all, there is no POSIX way of doing, but there are reasonable
alternatives.

1. Using `readlink` and `dirname`

```bash
#!/bin/sh
SCRIPT=$(readlink -f "$0")
SCRIPTDIR=$(dirname "$SCRIPT")
echo "SCRIPTPATH='${SCRIPT}' SCRIPTDIR='${SCRIPTDIR}'"
```

This should work in plain `sh`, but requires `readlink`, which can also
resolve symlinks.

2. Using `BASH_SOURCE`

```bash
#!/bin/bash
SCRIPTDIR=$(dirname "${BASH_SOURCE[0]}")
echo "BASH_SOURCE='${BASH_SOURCE}' SCRIPTDIR='${SCRIPTDIR}'"
```

Quick and dirty, but works.

### Best way to perfectly forward command line arguments

The only way to forward the command line arguments as they were originally sent
is to quote the args array like this:

```bash
#!/bin/bash
somecommand "$@"
```

([ref](https://stackoverflow.com/questions/3008695/what-is-the-difference-between-and-in-bash/14247867#14247867))

## Python

### Preferred way to call Python

UNIX:

```bash
python some_python2_script.py
python3 some_python3_script.py
```

Windows/Cygwin:

```bash
py -2 some_python2_script.py
py -3 some_python3_script.py
```

When calling modules as script, `py` can sometimes fail.  Tough.

### Installing or updating Python package

```bash
pip3 install -U some_package_name
```

### Format a string in Python

It used to be:

```python
'Hello %s %s' % ('John', 'Smith')
```

But this is now the preferred solution:

```python
'Hello {0} {1}'.format('John', 'Smith')
```

You can actually use a `dict`:

```python
data = { 'first': 'John', 'last': 'Smith' }
'Hello {first} {last}'.format(**data)
```

([ref](https://pyformat.info/))

# Web Tools

## Curl

### How to POST with JSON data

For example, to create data in a CRUD API.

```bash
curl -k "$(HOST)/bookmark" \
     -d '{"uri":"http://google.com/","name":"Google","info":"My usual search engine.","tags":["search","common"]}'
```

## WGet

### Mirror whole site

```bash
wget --mirror --convert-links --adjust-extension --page-requisites --no-parent http://somesite.dom/some/page/
```

or simply:

```base
wget -mkEpnp http://somesite.dom/some/page/
```

([credits](https://www.guyrutenberg.com/2014/05/02/make-offline-mirror-of-a-site-using-wget/))

### Download all MP3 files listed in a page

```bsh
wget -r -l1 -H -t1 -nd -N -np -A.mp3 -erobots=off http://site.dom/page.html
```

## Others

### Quick and dirty web server

Just go in the directory you want to serve, and type:

```bash
python3 -m http.server 8000
```

([doc](https://docs.python.org/3/library/http.server.html))

or for Python 2:

```bash
python -m SimpleHTTPServer 8000
```

([doc](https://docs.python.org/2/library/simplehttpserver.html))

# Image Tools

## GIMP

### Convert PNG to premultiplied alpha

1. Load the image with GIMP. In the layer window, name the image layer `image`.
2. Right-click the layer and select **Add Layer Mask**. Select **Transfer layer's alpha channel**, click add.
3. Create a new layer by hitting the **Create new layer** button in the bottom left of the layer window, call this layer `alpha`
4. Left click on the alpha mask of the `image` layer (white border around right thumbnail) ➜ `Ctrl`+`C`.
5. Left click on the `alpha` layer ➜ `Ctrl`+`V`.
6. Right click on the new temporary layer that is created, select **Anchor Layer**.
7. Create a new layer with black (`0x000000`) and call the layer `background`.
8. Order the layer like this (by left clicking and dragging the layers around):
   - alpha
   - image
   - background
9. Right-click the `image` layer and choose **Merge Down**.
10. Right-click the resulting layer and select **Add Layer Mask**. Select **White (full opacity)**, click add.
11. Select the `alpha` layer ➜ `Ctrl`+`C` ➜ select alpha mask of `background` layer ➜ `Ctrl`+`V`.
12. Right-click on the new temporary layer, select **Anchor Layer**.
13. Delete the `alpha` layer.
14. Save file as PNG.

## Inkscape

### Convert SVG to PNG

```bash
inkscape  --without-gui --export-area-drawing --file=some_input_file.svg --export-png=some_output_file.png
```

### Convert SVG to PDF

```bash
inkscape  --without-gui --export-area-drawing --file=some_input_file.svg --export-pdf=some_output_file.pdf
```

# Containers

## Docker

### Remove images that haven't completed

```bash
docker rmi -f $(docker images -q --filter "dangling=true")
```

### Run a shell in a Docker image

```bash
docker run -it genvidtech/nativesdk /bin/sh
```

### Run a shell in the first dangling image

```bash
docker run -it $(docker images -q --filter "dangling=true" | head -1) /bin/sh
```

### Delete all of the unused images

```bash
docker system prune --all
```

### Other useful routines

```bash
# Removes all dangling images.
function dcd {
    imgs=$(docker images -q --filter "dangling=true")
    if [ ! -z $imgs ]; then
        docker rmi -f ${imgs}
    else
        echo "ERROR: No dangling image..."
    fi
}
```

```bash
# Opens a shell to the specified image.
function dss {
    if [ ! -z $1 ]; then
        docker run -it ${1} /bin/sh
    else
        echo "ERROR: Missing container ID."
    fi
}
```

```bash
# Opens a shell to the latest dangling image.
function dsd {
    img=$(docker images -q --filter "dangling=true" | head -1)
    if [ ! -z $img ]; then
        dss ${img}
    else
        echo "ERROR: No dangling image..."
    fi
}
```

# Mac OS

## QuickView a file

Just hit `Space` with pretty much any files selected in the Finder.

For easy fullscreen slideshows: `Opt`+`Space`.

## Open up a file or folder

```bash
open <file_or_folder>
```

If you open up a file, it's default editor will launch.
For folders, it'll open in the Finder.
For application, they'll get launched.

See its [man page](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man1/open.1.html) for more details.

## Quick way to get NTFS read+write mounts

First, check name and path of the drive you want to remount read+write:

```bash
mount
```

Let's assume it's `/dev/disk3s1` on `/Volumes/SOMEDRIVE`.

The, remount it in read+write mode:

```bash
sudo umount /Volumes/SOMEDRIVE
mkdir /Volumes/SOMEDRIVE
sudo mount -t ntfs -o rw,auto,nobrowse /dev/disk3s1 /Volumes/SOMEDRIVE
```

## Finding/deleting most OSX-specific meta files

The Finder creates various hidden files.

We used to be able to [prevent their creation](http://hints.macworld.com/article.php?story=2005070300463515) with the following setting:

```bash
defaults write com.apple.desktopservices DSDontWriteNetworkStores true
```

but I don't know if it still works.

In case they are created, we can find many of them by searching for files starting with `._` and checking that they are small:

```bash
find . -name "._*" -size -5k
find . -name .DS_Store
```

Add `-delete` at the end if you want to get rid of them.

OSX also has a [dot_clean](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man1/dot_clean.1.html) utility, but I've never used it.

# Windows

## Networking

### Change the order of your preferred Wi-Fi networks

First, look at the list using an admin command prompt (e.g. `Win`+`X`):

```batch
netsh wlan show profiles
```

Then, reorder to your liking, say, moving `MyWiFiName` to the second spot:

```batch
netsh wlan set profileorder interface="Wi-Fi" name="MyWiFiName" priority=2
```

### Limit bandwidth used on certain Wi-Fi networks

This is useful, for example, when using hotspot from a cell phone.

You can specify in your Wi-Fi's Properties that it is a metered connection.

In an admin command line, this would do the same thing:

```batch
netsh wlan set profileparameter name="MyWiFiName" cost="Fixed"
```

To revert, use `Unrestricted`:

```batch
netsh wlan set profileparameter name="MyWiFiName" cost="Unrestricted"
```

The `Variable` value is also possible, but Win10 uses `Fixed`, so that's what I use as well.

To see the final result:

```batch
netsh wlan show profiles name="MyWiFiName"
```

### Control Samba shares from the command-line

To view current network shares:

```batch
net use
```

Note that you need to be a **non**-admin to see user mounts.

To mount a simple share of the next available drive letter:

```batch
net use * "\\server\my share" /persistent:no
```

To force a specific drive letter, using specific credentials, and have it mounted on next reboot:

```batch
net use M: "\\server\my media" somepasswd /user:somedomain\someuser /savecred /persistent:yes
```

I believe the following also works:

```batch
net use M: "\\server\my media" somepasswd /user:someuser@somedomain /savecred /p:yes
```

To delete a mount:

```batch
net use M: /delete
```

## Cygwin

### Create NTFS symlinks

```bash
CYGWIN="winsymlinks:nativestrict" ln -s src dst
```

This replaces `mklink` equivalents:

```batch
mklink dstFile srcFile
mklink /D dstDir srcDir
```

(from [here](https://stackoverflow.com/questions/18654162/enable-native-ntfs-symbolic-links-for-cygwin))

We used to require admin privileges (or `SeCreateSymbolicLinkPrivilege`), but it's [no longer required](https://blogs.windows.com/buildingapps/2016/12/02/symlinks-windows-10/), as long as you have Developer Mode enabled.

Here is a script to replace it for the `ln` tool:

```bash
#!/bin/sh
##
# A wrapper script to use native NTFS symlink with Cygwin's ln tool.
# Only works if the user has SeCreateSymbolicLinkPrivilege authority,
# e.g. in Windows 10 Creators Update with Developer Mode.
# https://blogs.windows.com/buildingapps/2016/12/02/symlinks-windows-10/
##

CYGWIN="winsymlinks:nativestrict" /usr/bin/ln "$@"
```

Using this script is the preferred approach, since setting that variable breaks ZShell's history (it yields a permission error when launching a new shell) when put in `.zshrc`.

### Using native Windows Perforce client under Cygwin

I used to have problems launching `p4` from a Cygwin shell, but the following script fixed it:

```bash
#!/bin/bash
export PWD=$(cygpath --windows --absolute .)
exec "/cygdrive/c/Program Files/Perforce/p4" "$@"
```
