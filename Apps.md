# Applications

These are some of my favorite applications.

[TOC]

# Compression

## [Bandizip](http://www.bandisoft.com/bandizip/)

Platform: **Windows**

I configure double click to be "Extract Here (Smart)", and almost never have to open up an archive.

Recommend setting compression level to max.

## [The Unarchiver](https://theunarchiver.com/)

Platform: **MacOS**

Just associate to all formats.

Faster than Apple's native Archive Utility, especially for `tar.gz`.

# Internet

## [Chrome](https://www.google.com/chrome/) / [Chromium](https://www.chromium.org/getting-involved/download-chromium)

Platform: **Android**, **iOS**, **MacOS**, **UNIX**, **Windows**

Probably the most secure browser out there.

## [Firefox](https://www.mozilla.org/en-US/firefox/)

Platform: **Android**, **MacOS**, **UNIX**, **Windows**

A good web browser, with a great history.

Has ad-blocking under [Android](https://play.google.com/store/apps/details?id=org.mozilla.firefox).

## [Thunderbird](https://www.mozilla.org/en-US/thunderbird/)

Platform: **MacOS**, **UNIX**, **Windows**

Best cross-platform email client.

Has a [portable version](https://portableapps.com/apps/internet/thunderbird_portable), which makes switching machines really easy (just drag'n'drop a single folder).

# Multimedia

## Image

### [Gimp](https://www.gimp.org/)

Platform: **MacOS**, **UNIX**, **Windows**

A free alternative to Photoshop.

On first start, set to `Single-Window Mode` in the `Windows` menu, then maximize the window.

Recommend installing [G'MIC](http://gmic.eu/) for advanced image processing filters.

### [Inkscape](https://inkscape.org/en/)

Platform: **MacOS**, **UNIX**, **Windows**

A free alternative to Illustrator.

Good for working scalable/vectorial graphics, including diagrams.

### [Irfan View](http://www.irfanview.com/)

A simple and fast image viewer.

## Video

### [FFMPEG](https://www.ffmpeg.org/)

Platform: **MacOS**, **UNIX**, **Windows**

[Zeranoe builds](https://ffmpeg.zeranoe.com/builds/)

The most complete audio/video library available.

Can be used to extract, convert, fix pretty much any audio and video format in existence.

### [Handbrake](https://handbrake.fr/)

Platform: **MacOS**, **UNIX**, **Windows**

Easy interface for ripping and converting video.

### [VideoLAN Client (VLC)](https://www.videolan.org/)

Platform: **MacOS**, **UNIX**, **Windows**

Can open pretty much any audio/video file.

# Office/Productivity

## [LibreOffice](https://www.libreoffice.org/)

Platform: **MacOS**, **UNIX**, **Windows**

An open source office suite, free replacement for Microsoft Word, Excel, PowerPoint.

# Utility

## [ConEmu](https://conemu.github.io/)

Platform: **Windows**

A good terminal emulator for Windows.

I launch Cygwin ZShell terminals using the following task:

```batch
set CHERE_INVOKING=1 & %ConEmuDrive%\cygwin\bin\zsh.exe --login -i -new_console:C:"%ConEmuDrive%\cygwin\Cygwin.ico"
```

## [Executor](http://executor.dk/)

Platform: **Windows**

An alternative launcher.

No longer in development, but can be made to work in Windows 10 (I use `Ctrl`+`Shift`+`Space` for the shortcut).

Useful for a quick calculator by prefixing the command with `#`.

## [FreeFileSync](https://www.freefilesync.org/)

Platform: **MacOS**, **UNIX**, **Windows**

Good GUI to sync or mirror directories.

Replaced [rsync](https://rsync.samba.org) for me.

## [Greenshot](http://getgreenshot.org/)

Platform: **Windows**

The best screenshot capture utility for Windows.

Hit `Print Screen`, then `Space`, select your window, and save to PNG.

I always open in the Image Editor and save as a separate step (sometimes, I copy to clipboard and send in instant messaging).

Can capture the mouse pointer as a separate layer (which can also be deleted right before saving).

Can also add step numbers easily (for tutorial images).

## [WinDirStat](https://windirstat.net/)

Platform: **Windows**

A utility to figure out what takes disk space.